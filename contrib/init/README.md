Sample configuration files for:

SystemD: directd.service
Upstart: directd.conf
OpenRC:  directd.openrc
         directd.openrcconf
CentOS:  directd.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
