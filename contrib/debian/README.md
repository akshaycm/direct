
Debian
====================
This directory contains files used to package directd/direct-qt
for Debian-based Linux systems. If you compile directd/direct-qt yourself, there are some useful files here.

## direct: URI support ##


direct-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install direct-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your directqt binary to `/usr/bin`
and the `../../share/pixmaps/direct128.png` to `/usr/share/pixmaps`

direct-qt.protocol (KDE)

